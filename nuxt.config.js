import colors from 'vuetify/es5/util/colors'

export default {
  ssr: true,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Home',
    title: 'Tiktok management',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vue-awesome-swiper.js', mode: 'client' },
    { src: '~/plugins/fb-sdk.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components/header',
      '~/components/footer',
      '~/components/common',
      '~/components/menu',
      '~/components/side-bar',
      '~/components/card',
    ],
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/i18n',
    '@nuxtjs/device',
  ],

  device: {
    refreshOnResize: true,
  },

  router: {
    linkExactActiveClass: 'exact-active-link',
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/styles/overrides.scss'],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/styles/variables.scss'],
    treeShake: true,
    icons: false,
    family: process.env.FONT_NAME,
    defaultAssets: {
      font: {
        family: 'Roboto',
      },
      icons: false,
    },
    theme: {
      options: { customProperties: true },
      dark: false,
      themes: {
        light: {
          primary: '3D405B',
          red: 'F22548',
          pink: 'FAE6E9',
          neutral: 'FCF7F8',
          gray: 'FAFAFA',
          gray2: 'BEBEC2',
          purple: 'F1F2F6',
          white: 'FFFFFF',
          violet: 'E6E8FA',
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  i18n: {
    locales: [
      {
        code: 'vi',
        name: 'Vietnamese',
        file: 'vi-VN.js',
        icon: '/icons/ic-vn-flag.svg',
      },
      {
        code: 'en',
        name: 'English',
        file: 'en-UK.js',
        icon: '/icons/ic-uk-flag.svg',
      },
    ],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'en',
    vueI18n: {
      numberFormats: {
        en: {
          currency: {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0, // set fraction digits to 0 to remove cents
            maximumFractionDigits: 2,
          },
        },
      },
    },
  },
}
