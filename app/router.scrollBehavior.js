export default function (to, from, savedPosition) {
  if (to.hash) {
    return { offset: { y: 64 }, selector: to.hash, behavior: 'smooth' }
  } else if (savedPosition) {
    return savedPosition
  } else {
    return { x: 0, y: 0 }
  }
}
