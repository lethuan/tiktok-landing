import Vue from 'vue'

new Vue({
  created() {
    ;(function (d, s, id) {
      var js
      var fjs = d.getElementsByTagName(s)[0]
      if (d.getElementById(id)) return
      js = d.createElement(s)
      js.id = id
      js.src =
        '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId={{$store.state.facebookAppId}}'
      fjs.parentNode.insertBefore(js, fjs)
    })(document, 'script', 'facebook-jssdk')
  },
})
