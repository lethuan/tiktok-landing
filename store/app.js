export const state = () => ({
  headerWhite: false,
})

export const getters = {}

export const mutations = {
  setHeader(state, headerWhite) {
    state.headerWhite = headerWhite
  },
}

export const actions = {
  FB_parse() {
    setTimeout(() => {
      if (window && window.FB) {
        window.FB.XFBML.parse()
      }
    }, 500)
  },
}
